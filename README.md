# Sunshine

Shine some light on your TV shows collection!

## Features

- Add shows, powered by [The Movie DB](https://www.themoviedb.org)
- Mark shows, seasons or episodes as watched
- View recently released episodes
- Powered by Semantic Web technology such as [RDF](https://www.w3.org/TR/rdf11-concepts/), and [schema.org](https://schema.org)
- Adaptive UI, optimized for low power devices such as the [Pine64 PinePhone](https://pine64.org/pinephone)

## Screenshots

![New view](https://sunshine.dylanvanassche.be/screenshots/new.png "New episodes are listed chronological.")
![Library view](https://sunshine.dylanvanassche.be/screenshots/library.png "All shows appears in your library.")
![Show view](https://sunshine.dylanvanassche.be/screenshots/show.png "View episodes, seasons, description of each show.")
![Episodes view](https://sunshine.dylanvanassche.be/screenshots/episodes.png "Check all episodes of a show and mark the ones you watched.")

# FAQ

## Why is this application named 'Sunshine'?

This application is named after 
[my best friend Ellen Marcelis, whom I lost in 2020](https://dylanvanassche.be/blog/2020/goodbye-my-friend/).
To honor her, I named this app 'Sunshine' as her name means 'light' or 'sun ray'.

# Dependencies

## Python modules

- `urllib3`
- `certifi`
- `charset_normalizer`
- `idna`
- `requests`
- `appdirs`
- `attrs`
- `cattrs`
- `url-normalize`
- `requests-cache`
- `isodate`
- `pyparsing`
- `rdflib`
- `PyXDG`

## GTK

- `gtk3.0+`
- `libhandy1`

# License

Sunshine is released under the GPLv3 license.
