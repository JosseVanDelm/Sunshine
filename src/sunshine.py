# sunshine.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from rdflib import Namespace

APP_VERSION = '0.1.0'
APP_ID = 'be.dylanvanassche.sunshine'

# Alphabetically ordered, feel free to add yourself here
# when you contribute to Sunshine
APP_AUTHORS = [
    'Dylan Van Assche',
    'Ellen Marcelis',
    'Jordan Petridis'
]
APP_ARTISTS = [
    'Tobias Bernard'
]

# Constraint cover height
COVER_WIDTH = -1
COVER_HEIGHT = 150

STORAGE_DIR = 'sunshine'
STORAGE_DIR_SHOW = 'show'
STORAGE_DIR_SEASON = 'season'
STORAGE_DIR_EPISODE = 'episode'
STORAGE_DIR_COVER = 'cover'
BASE_URL = 'https://sunshine.dylanvanassche.be'
RDF_FORMAT= 'ntriples'

SCHEMA = Namespace('http://schema.org/')
