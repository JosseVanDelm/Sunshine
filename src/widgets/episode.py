# episode.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, Handy
from datetime import datetime
from typing import Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from ..backend.episode import Episode
    from ..backend.storage import Storage


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/episode_widget.ui')
class EpisodeWidget(Handy.ActionRow):
    __gtype_name__ = 'Episode'

    title_label: Gtk.Widget = Gtk.Template.Child()
    date_label: Gtk.Widget = Gtk.Template.Child()
    description_label: Gtk.Widget = Gtk.Template.Child()
    watched_checkmark: Gtk.Widget = Gtk.Template.Child()
    watched_button: Gtk.Widget = Gtk.Template.Child()

    def __init__(self, episode: Episode, storage: Storage, **kwargs):
        super().__init__(**kwargs)
        self._episode: Episode = episode
        self._storage: Storage = storage

        self.title_label.set_label(self._episode.title)
        if self._episode.date is not None:
            date: str = datetime.strftime(self._episode.date, "%d/%m/%Y")
            self.date_label.set_label(f'{date}')

        self.refresh()

        self.connect('activated', self._toggle_description)
        self.description_label.set_visible(False)

        self.watched_button.connect('clicked', self._edit_watch_state)

    def refresh(self):
        # Unaired episodes from the future cannot be marked as watched
        if self._episode.date is not None and self._episode.date > datetime.now():
            self.watched_checkmark.set_visible(False)
            self.watched_button.set_visible(False)
            self.title_label.get_style_context().add_class('dim-label')
            self.description_label.get_style_context().add_class('dim-label')
            return

        # Dim the episode widget when watched and add checkmark
        if self._episode.watched:
            self.watched_checkmark.set_visible(True)
            self.watched_button.set_visible(False)
            self.title_label.get_style_context().add_class('dim-label')
            self.description_label.get_style_context().add_class('dim-label')
        # Hide checkmark and remove dim
        else:
            self.watched_checkmark.set_visible(False)
            self.watched_button.set_visible(True)
            self.title_label.get_style_context().remove_class('dim-label')
            self.description_label.get_style_context().remove_class('dim-label')

    def _toggle_description(self, row):
        if self._episode.description is not None and self._episode.description:
            self.description_label.set_label(self._episode.description)
            self.description_label \
            .set_visible(not self.description_label.get_visible())

    def _edit_watch_state(self, button):
        self._episode.watched = not self._episode.watched
        self.refresh()
