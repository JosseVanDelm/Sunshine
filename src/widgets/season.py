# season.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, Handy
from typing import Optional, TYPE_CHECKING

from ..views.episodes import EpisodesView

if TYPE_CHECKING:
    from ..backend.season import Season
    from ..backend.storage import Storage
    from ..page_stack import PageStack


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/season_widget.ui')
class SeasonWidget(Handy.ActionRow):
    __gtype_name__ = 'Season'

    season_container: Gtk.Widget = Gtk.Template.Child()
    title_label: Gtk.Widget = Gtk.Template.Child()
    number_of_episodes_label: Gtk.Widget = Gtk.Template.Child()
    watched_checkmark: Gtk.Widget = Gtk.Template.Child()

    def __init__(self, season: Season, page_stack: PageStack, storage: Storage, **kwargs):
        super().__init__(**kwargs)
        self._season: Season = season
        self._page_stack: PageStack = page_stack
        self._storage: Storage = storage

        self.title_label.set_label(self._season.title)

        if self._season.number_of_episodes > 0:
            self.number_of_episodes_label \
            .set_label(f'{self._season.number_of_episodes} episodes')
            self.connect('activated', self.show_episodes)
        else:
            self.number_of_episodes_label \
            .set_label('No episodes yet')
            self.title_label.get_style_context().add_class('dim-label')

        self.refresh()

    def show_episodes(self, row):
        if self._season.number_of_episodes > 0:
            episodes = EpisodesView(self._season, self._storage)
            self._page_stack.push(episodes, 'episodes', is_subview=True)

    def refresh(self):
        # Dim the episode widget when watched and add checkmark
        if self._season.watched:
            self.watched_checkmark.set_visible(True)
            self.title_label.get_style_context().add_class('dim-label')
        # No episodes yet, season is announced
        elif self._season.number_of_episodes == 0:
            self.watched_checkmark.set_visible(False)
            self.title_label.get_style_context().add_class('dim-label')
        # Hide checkmark and remove dim
        else:
            self.watched_checkmark.set_visible(False)
            self.title_label.get_style_context().remove_class('dim-label')
