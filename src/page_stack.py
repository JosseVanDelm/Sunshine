# page_stack.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Handy
from typing import List, Optional
from logging import debug, error

MOBILE_DISPLAY_WIDTH = 360
MOUSE_BACK_BUTTON = 8


class PageStack:
    """
    A general PageStack for GTK applications to provide proper navigation
    including keyboard and mouse buttons.
    """
    def __init__(self, window: Gtk.ApplicationWindow, squeezer: Handy.Squeezer,
                 headerbar: Handy.HeaderBar, views: Gtk.Stack,
                 navigation_buttons: Gtk.Stack, modify_buttons: Gtk.Stack,
                 top_switcher: Handy.ViewSwitcher,
                 bottom_switcher: Handy.ViewSwitcherBar):
        """
        Creates a PageStack with a view stack and navigation stacks.
        """
        self._window: Gtk.ApplicationWindow = window
        self._squeezer: Handy.Squeezer = squeezer
        self._headerbar: Handy.HeaderBar = headerbar
        self._views: Gtk.Stack = views
        self._navigation_buttons: Gtk.Stack = navigation_buttons
        self._modify_buttons: Gtk.Stack = modify_buttons
        self._top_switcher: Handy.ViewSwitcher = top_switcher
        self._bottom_switcher: Handy.ViewSwitcherBar = bottom_switcher
        self._subviews: List[str] = []
        self._previous_views: List[str] = []

        self._back_button: Gtk.Button = self._navigation_buttons \
                                            .get_child_by_name('back')
        self._back_button.set_sensitive(True)
        self._back_button.connect('clicked', self._process_back_button)
        self._loading_indicator: Gtk.Spinner = \
            self._navigation_buttons.get_child_by_name('loading_indicator')
        self._gesture: Gtk.GestureMultiPress = \
            Gtk.GestureMultiPress.new(self._window)
        self._gesture.set_button(0) # Listen for all mouse button events
        self._gesture.set_propagation_phase(Gtk.PropagationPhase.CAPTURE)
        self._gesture.connect('pressed', self._process_mouse_event)

        self._squeezer.connect('notify::visible-child',
                               self._process_headerbar_width_change)
        self._headerbar.connect('size-allocate',
                                self._process_view_width_change)

    def current_view_name(self) -> str:
        return self._views.get_visible_child_name()

    def current_view(self) -> Gtk.Widget:
        return self._views.get_visible_child()

    def show(self, name: str, hide_navigation: bool = False):
        """
        Shows an existing view in the stack.
        Optionally hides the navigation.
        """
        mobile: bool = self._window.get_size()[0] <= MOBILE_DISPLAY_WIDTH

        self._views.set_visible_child_name(name)

        if hide_navigation:
            self._top_switcher.set_visible(False)
            self._bottom_switcher.set_reveal(False)
        else:
            self._bottom_switcher.set_reveal(mobile)
            self._top_switcher.set_visible(not mobile)

    def push(self, view: Gtk.Widget, name: str, is_subview: bool = False,
             title: Optional[str] = None, icon: Optional[str] = None):
        """
        Adds a view to the stack and makes it the visible.
        If the view is a subpage, the back button will be shown
        and the main navigation will be hidden.
        """
        mobile: bool = self._window.get_size()[0] <= MOBILE_DISPLAY_WIDTH

        debug(f'Push view {name}')

        if is_subview:
            self._subviews.append(name)
            self._navigation_buttons.set_visible_child_name('back')
            self._modify_buttons.set_visible_child_name('edit')
            self._top_switcher.set_visible(False)
            self._bottom_switcher.set_reveal(False)
        else:
            self._bottom_switcher.set_reveal(mobile)
            self._top_switcher.set_visible(not mobile)

        self._previous_views.append(self._views.get_visible_child_name())

        # Always change the view at the end to avoid glitches
        # for low power devices
        self.add(view, name, is_subview, title, icon)
        self.show(name, is_subview)

    def pop(self, name: Optional[str] = None):
        """
        Removes a view of the stack
        and makes the previous view visible again.
        If no view name is provided, the current visible view is popped.
        """
        current_view: Gtk.Widget = self._views.get_visible_child()
        previous_view_name: str = self._previous_views.pop()
        mobile: bool = self._window.get_size()[0] <= MOBILE_DISPLAY_WIDTH

        if name is None:
            name = current_view.get_name()
        debug(f'Pop view {name}')

        if not previous_view_name in self._subviews:
            self._navigation_buttons.set_visible_child_name('add')
            self._modify_buttons.set_visible_child_name('menu')
            self._bottom_switcher.set_reveal(mobile)
            self._top_switcher.set_visible(not mobile)

        # Always change the view at the end to avoid glitches
        # for low power devices
        self._views.set_visible_child_name(previous_view_name)
        self._views.get_visible_child().refresh()
        self._views.remove(current_view)

    def add(self, view: Gtk.Widget, name: str, is_subview: bool = False,
            title: Optional[str] = None, icon: Optional[str] = None):
        """
        Adds a view to the stack.
        """
        debug(f'Add view {name}')
        if title is not None:
            self._views.add_titled(view, name, title)
        else:
            self._views.add_named(view, name)

        if icon is not None:
            self._views.child_set_property(view, 'icon-name', icon)

    def loading(self, is_loading: bool):
        if is_loading:
            self._loading_indicator.start()
            self._navigation_buttons.set_visible_child_name('loading_indicator')
        else:
            self._loading_indicator.stop()
            if self._views.get_visible_child_name() in self._subviews:
                self._navigation_buttons.set_visible_child_name('back')
                self._modify_buttons.set_visible_child_name('edit')
            else:
                self._navigation_buttons.set_visible_child_name('add')
                self._modify_buttons.set_visible_child_name('menu')

    def _process_back_button(self, button: Gtk.Button):
        """
        Handle headerbar back button.
        """
        if self._views.get_visible_child_name() in self._subviews:
            self.pop()
        else:
            error('Cannot go back when not in subview')

    def _process_mouse_event(self, gesture, n_press, x, y):
        """
        Handle mouse back button.
        """
        button: int = gesture.get_current_button()

        # Only allow to go back when back button is enabled
        if (button == MOUSE_BACK_BUTTON and
            self._views.get_visible_child_name() in self._subviews):
            self.pop()

    def _process_view_width_change (self, headerbar, allocation):
        """
        Force the viewswitcher to show up at the bottom when
        window < 360px (mobile).
        """
        # FIXME: This is a hack and should be removed when porting to GTK4
        mobile: bool = allocation.width <= MOBILE_DISPLAY_WIDTH

        if (not self._bottom_switcher.get_reveal() and
            not self._top_switcher.get_visible()):
            return

        self._bottom_switcher.set_reveal(mobile)
        self._top_switcher.set_visible(not mobile)


    def _process_headerbar_width_change(self, squeezer, event):
        """
        Switch when necessary between header bar at the top or bottom
        depending on screen size.
        """
        child: Gtk.Widget

        if (not self._bottom_switcher.get_reveal() and
            not self._top_switcher.get_visible()):
            return

        child = squeezer.get_visible_child()
        self._bottom_switcher.set_reveal(child != self._top_switcher)
            
