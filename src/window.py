# window.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, Handy, Gio, GLib
from typing import Optional, List, TYPE_CHECKING
from logging import debug, info, error
from threading import Thread

from .sunshine import APP_ID, APP_VERSION, APP_AUTHORS, APP_ARTISTS
from .views.new import NewView
from .views.library import LibraryView
from .add_new_show import AddNewShowDialog
from .page_stack import PageStack
from .backend.local import LocalStorage
from .backend.kg import KG
from .backend.show import Show

if TYPE_CHECKING:
    from .backend.storage import Storage
    from .backend.datasource import Datasource
    from rdflib import URIRef


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/window.ui')
class ShowsWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'ShowsWindow'

    Handy.init()
    squeezer: Gtk.Widget = Gtk.Template.Child()
    headerbar: Gtk.Widget = Gtk.Template.Child()
    headerbar_switcher: Gtk.Widget = Gtk.Template.Child()
    bottom_switcher: Gtk.Widget = Gtk.Template.Child()
    add_button: Gtk.Widget = Gtk.Template.Child()
    menu_button: Gtk.Widget = Gtk.Template.Child()
    edit_button: Gtk.Widget = Gtk.Template.Child()
    menu: Gio.Menu = Gtk.Template.Child()
    stack: Gtk.Stack = Gtk.Template.Child()
    navigation_button_stack: Gtk.Stack = Gtk.Template.Child()
    modify_button_stack: Gtk.Stack = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.add_button.connect('clicked', self.add_new_show_action)
        self.add_button.set_sensitive(True)
        self.menu_button.set_sensitive(True)
        self.edit_button.set_sensitive(True)
        self._new: Optional[Gtk.Widget] = None
        self._library: Optional[Gtk.Widget] = None
        self._page_stack: PageStack
        self._storage: Storage = LocalStorage.get_instance()
        self._datasource: Datasource = KG()

        self.setup_actions()
        self.setup_navigation()

    def add_new_show_action(self, button):
        """
        Show Add New Show dialog to add a new show to the library
        """
        iri_list: List[URIRef] = self._storage.retrieve_shows()
        dialog: AddNewShowDialog = AddNewShowDialog(iri_list)
        response: Gtk.ResponseType = dialog.run()
        selected_show: Show = dialog.selected_show
        dialog.destroy()

        if response == Gtk.ResponseType.OK:
            info(f'Adding show: {selected_show.title} '
                 f'({selected_show.iri})')
            self._page_stack.loading(True)
            self._new.loading(True)
            self._library.loading(True)
            thread: Thread = Thread(target=self.add_show,
                                    args=(selected_show.iri,))
            thread.daemon = True
            thread.start()
        elif (response == Gtk.ResponseType.CANCEL or
              response == Gtk.ResponseType.NONE):
            info('Cancel adding show operation')
        else:
            error(f'Unknown response for adding show operation: {response}')

    def setup_actions(self):
        """
        Setup actions from the menus
        """
        actions = {
            'about': self.show_about_dialog,
            'mark-watched': self.edit_watch_state,
            'refresh-shows': self.refresh_shows_action
        }

        for key, value in actions.items():
            action = Gio.SimpleAction.new(key, None)
            action.connect('activate', value)
            self.add_action(action)

    def _update_widgets(self, show: Show):
        self._library.add_show(show)
        self._new.add_show(show)
        return False

    def _replace_widgets(self, show: Show):
        self._library.replace_show(show)
        self._new.replace_show(show)
        return False

    def _update_loading_indicator(self):
        self._page_stack.loading(False)
        self._new.loading(False)
        self._library.loading(False)
        return False

    def add_show(self, iri: URIRef):
        show: Show = self._datasource.show(iri)
        GLib.idle_add(self._update_widgets, show)
        GLib.idle_add(self._update_loading_indicator)

    def load_shows(self, **kwargs):
        iri_list: List[URIRef] = self._storage.retrieve_shows()

        for iri in iri_list:
            show: Show = Show(iri)
            show.prefetch()
            GLib.idle_add(self._update_widgets, show)

        GLib.idle_add(self._update_loading_indicator)

    def refresh_shows(self, **kwargs):
        iri_list: List[URIRef] = self._storage.retrieve_shows()

        for iri in iri_list:
            show: Show = self._datasource.show(iri, refresh=True)
            show.prefetch()
            GLib.idle_add(self._replace_widgets, show)

        GLib.idle_add(self._update_loading_indicator)

    def setup_navigation(self):
        """
        Setup navigation PageStack.
        """
        self._page_stack = PageStack(self, self.squeezer, self.headerbar,
                                     self.stack, self.navigation_button_stack,
                                     self.modify_button_stack,
                                     self.headerbar_switcher,
                                     self.bottom_switcher)

        # New view displays the newest unwatched episodes
        self._new = NewView(self._storage)
        self._new.loading(True)
        self._page_stack.add(self._new, 'new', title='New',
                             icon='emoji-recent-symbolic')

        # Library view lists all shows in the library
        self._library = LibraryView(self._page_stack, self._storage)
        self._library.loading(True)
        self._page_stack.add(self._library, 'library', title='Library',
                             icon='accessories-dictionary-symbolic')

        # Show newest episodes at start up when we already have some shows
        if self._storage.has_shows:
            self._page_stack.show('new')
        else:
            self._page_stack.show('library')

        # Load shows in separate thread
        self._page_stack.loading(True)
        thread: Thread = Thread(target=self.load_shows)
        thread.daemon = True
        thread.start()

    def show_about_dialog(self, action: Gio.SimpleAction,
                          params: Optional[GLib.Variant]):
        """
        Show about dialog.
        """
        dialog = Gtk.AboutDialog()
        dialog.set_authors(APP_AUTHORS)
        dialog.set_artists(APP_ARTISTS)
        dialog.set_logo_icon_name(APP_ID)
        dialog.set_license_type(Gtk.License.GPL_3_0)
        dialog.set_program_name(_('Sunshine'))
        dialog.set_translator_credits(_('translator-credits'))
        dialog.set_version(APP_VERSION)
        dialog.set_comments(_('Shines some light on your TV shows collection.'
                              '\nThis program was written to honor '
                              'Ellen Marcelis (1997-2020).'))
        dialog.set_website('https://gitlab.com/DylanVanAssche/Sunshine')
        dialog.set_website_label('Source code')
        dialog.set_copyright('© 2021-2022 Dylan Van Assche')
        dialog.run()
        dialog.destroy()

    def edit_watch_state(self, action: Gio.SimpleAction,
                         params: Optional[GLib.Variant]):
        view: Gtk.Widget

        view = self._page_stack.current_view()
        if self._page_stack.current_view_name() == 'show':
            debug('Mark show as watched')
            view.watched(True)

        elif self._page_stack.current_view_name() == 'episodes':
            debug('Mark season as watched')
            view.watched(True)

    def refresh_shows_action(self, action: Gio.SimpleAction,
                             params: Optional[GLib.Variant]):
        self._page_stack.loading(True)
        self._new.loading(True)
        self._library.loading(True)
        thread: Thread = Thread(target=self.refresh_shows)
        thread.daemon = True
        thread.start()
