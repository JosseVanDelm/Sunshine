<?xml version="1.0" encoding="UTF-8"?>
<!--

Copyright (C) 2021 - 2022

This file is part of Sunhine and was modified from GNOME Podcasts
under the GPLv3 license:

Sunshine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sunshine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sunshine.  If not, see <http://www.gnu.org/licenses/>.

Authors:
Dylan Van Assche

-->
<interface>
  <requires lib="gtk+" version="3.22"/>
  <requires lib="libhandy" version="1.0"/>
  <!-- interface-license-type gplv3 -->
  <!-- interface-name Sunshine -->
  <!-- interface-description Shines some light on your TV shows collection -->
  <!-- interface-copyright 2021 - 2022 -->
  <!-- interface-authors Dylan Van Assche -->
  <template class="Episode" parent="HdyActionRow">
    <property name="activatable">True</property>
    <child>
      <!-- Container margins -->
      <object class="GtkVBox">
      <property name="visible">True</property>
      <property name="can_focus">False</property>
      <property name="valign">center</property>
      <property name="margin_top">6</property>
      <property name="margin_bottom">6</property>
      <property name="margin_right">6</property>
      <property name="margin_left">6</property>
      <property name="spacing">16</property>
      <child>
        <!-- Main container -->
        <object class="GtkBox">
          <property name="visible">True</property>
          <property name="can_focus">False</property>
            <child>
              <!-- Text container margins -->
              <object class="GtkBox">
                <property name="height_request">64</property>
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="valign">center</property>
                <property name="margin_start">6</property>
                <property name="margin_end">6</property>
                <property name="orientation">vertical</property>
                <child>
                  <!-- Info container margins -->
                  <object class="GtkBox">
                    <property name="visible">True</property>
                    <property name="can_focus">False</property>
                    <property name="margin_start">6</property>
                    <property name="margin_end">6</property>
                    <property name="margin_top">6</property>
                    <property name="margin_bottom">6</property>
                    <property name="hexpand">True</property>
                    <property name="vexpand">True</property>
                    <child>
                      <!-- Vertical container for info -->
                      <object class="GtkBox">
                        <property name="visible">True</property>
                        <property name="can_focus">False</property>
                        <property name="halign">start</property>
                        <property name="valign">center</property>
                        <property name="orientation">vertical</property>
                        <property name="spacing">6</property>
                        <child>
                          <!-- Title & checkmark container -->
                          <object class="GtkBox">
                            <property name="visible">True</property>
                            <property name="can_focus">False</property>
                            <property name="halign">start</property>
                            <property name="spacing">6</property>
                            <child>
                              <!-- Title label -->
                              <object class="GtkLabel" id="title_label">
                                <property name="visible">True</property>
                                <property name="can_focus">False</property>
                                <property name="label">Episode Title</property>
                                <property name="ellipsize">end</property>
                                <property name="single_line_mode">True</property>
                                <property name="track_visited_links">False</property>
                                <property name="lines">1</property>
                                <property name="xalign">0</property>
                              </object>
                              <!-- GtkLabel -->
                            </child>
                            <child>
                              <!-- Checkmark -->
                              <object class="GtkImage" id="watched_checkmark">
                                <property name="visible">True</property>
                                <property name="can_focus">False</property>
                                <property name="no_show_all">True</property>
                                <property name="tooltip_text" translatable="yes">You’ve already watched this.</property>
                                <property name="icon_name">object-select-symbolic</property>
                                <style>
                                  <class name="dim-label"/>
                                </style>
                              </object>
                              <!-- GtkImage -->
                            </child>
                          </object>
                          <!-- GtkBox -->
                        </child>
                        <child>
                          <object class="GtkLabel" id="date_label">
                            <property name="visible">True</property>
                            <property name="halign">start</property>
                            <property name="can_focus">False</property>
                            <property name="label"></property>
                            <property name="single_line_mode">True</property>
                            <property name="track_visited_links">False</property>
                            <attributes>
                              <attribute name="font-features" value="tnum=1"/>
                            </attributes>
                            <style>
                              <class name="dim-label"/>
                            </style>
                          </object>
                        </child>
                      </object>
                      <!-- GtkBox -->
                    </child>
                    <child>
                      <!-- Button container -->
                      <object class="GtkBox">
                        <property name="visible">True</property>
                        <property name="can_focus">False</property>
                        <property name="halign">end</property>
                        <property name="spacing">6</property>
                        <child>
                          <!-- Mark as watched button -->
                          <object class="GtkButton" id="watched_button">
                            <property name="can_focus">True</property>
                            <property name="receives_default">True</property>
                            <property name="no_show_all">True</property>
                            <property name="tooltip_text" translatable="yes">Mark as watched</property>
                            <property name="halign">center</property>
                            <property name="valign">center</property>
                            <child>
                              <!-- Button icon -->
                              <object class="GtkImage">
                                <property name="visible">True</property>
                                <property name="can_focus">False</property>
                                <property name="icon_name">object-select-symbolic</property>
                              </object>
                              <!-- GtkImage -->
                            </child>
                          </object>
                          <!-- GtkButton -->
                        </child>
                      </object>
                      <packing>
                        <property name="pack_type">end</property>
                      </packing>
                      <!-- GtkBox -->
                    </child>
                  </object>
                  <!-- GtkBox -->
                </child>
              </object>
              <!-- GtkBox -->
            </child>
          </object>
          <!-- GtkBox -->
        </child>
        <child>
          <!-- Hidden description -->
          <object class="GtkLabel" id="description_label">
            <property name="halign">start</property>
            <property name="xalign">0</property>
            <property name="label"></property>
            <property name="visible">False</property>
            <property name="can_focus">False</property>
            <property name="wrap">True</property>
            <property name="wrap_mode">word-char</property>
            <property name="ellipsize">end</property>
            <property name="justify">fill</property>
            <property name="lines">5</property>
            <property name="no_show_all">True</property>
            <attributes>
              <attribute name="font-features" value="tnum=1"/>
            </attributes>
          </object>
          <!-- GtkLabel -->
        </child>
      </object>
      <!-- GtkBox -->
    </child>
  </template>
</interface>
