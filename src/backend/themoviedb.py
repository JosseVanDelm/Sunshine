# themoviedb.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from datetime import datetime
from typing import List, Optional, TYPE_CHECKING
from logging import info, debug
from pathlib import Path
from requests import Session
from requests_cache import CachedSession
from rdflib import Graph, URIRef, Literal, RDF
from rdflib.namespace import XSD
from xdg import BaseDirectory

from .local import LocalStorage
from .datasource import Datasource
from .show import Show
from .season import Season
from .episode import Episode
from .search_result import SearchResult
from ..sunshine import SCHEMA, BASE_URL, STORAGE_DIR

if TYPE_CHECKING:
    from .storage import Storage

API_KEY = '19ff73fd64e28a86ec6348aa9f2431e2'
LANGUAGE = 'en-US'
INCLUDE_ADULT = False
API_BASE_URL = 'https://api.themoviedb.org/3'


class TheMovieDB(Datasource):
    """
    Datasource implementation of The Movie DB for retrieving TV shows
    information.
    """

    def __init__(self):
        super().__init__()
        self._poster_sizes: Optional[List[str]] = None
        self._image_base_url: Optional[str] = None
        self._storage: Storage = LocalStorage.get_instance()
        cache_path: Path = Path(BaseDirectory.save_cache_path(STORAGE_DIR))
        self._session: Session = CachedSession(str(cache_path))
        info(f'Cache installed at {cache_path}')

    def _configuration(self):
        """
        Retrieve The Movie DB API configuration
        """
        debug(f'Retrieving TheMovieDB API configuration')

        response = self._session.get(API_BASE_URL + '/configuration',
                                     params={'api_key': API_KEY})
        response.raise_for_status()
        response = response.json()

        if 'images' in response:
            self._poster_sizes = response['images'].get('poster_sizes')
            self._image_base_url = response['images'].get('secure_base_url')

    def search(self, query) -> List[Show]:
        """
        Search for TV shows on The Movie DB.
        """
        shows: List = []

        debug('Executing search against TheMovieDB API')

        if self._image_base_url is None or self._poster_sizes is None:
            self._configuration()

        response = self._session.get(API_BASE_URL + '/search/tv',
                                     params={'api_key': API_KEY,
                                             'language': LANGUAGE,
                                             'include_adult': INCLUDE_ADULT,
                                             'query': query})
        response.raise_for_status()
        response = response.json()

        if 'results' in response:
            for show in response['results']:
                iri: URIRef
                title: str
                date: Optional[datetime] = None

                # ID and title are required
                if not 'id' in show or not 'name' in show:
                    warning(f'Cannot add show without ID or title: {show}')
                    continue

                iri = URIRef(f'{BASE_URL}/show/{show["id"]}')
                title = str(show['name'])

                if show.get('first_air_date') is not None:
                    try:
                        date = datetime.strptime(show['first_air_date'],
                                                 '%Y-%m-%d')
                    except ValueError:
                        debug(f'Invalid date: "{show["first_air_date"]}" ' \
                              f'for show "{title}" ({iri})')

                shows.append(SearchResult(iri, title, date))
            return shows
        else:
            raise Exception('No results for query')

    def show(self, iri: URIRef):
        """
        Retrieve all information about a TV show by ID
        """
        raise NotImplementedError('The Movie DB datasource is being deprecated')
