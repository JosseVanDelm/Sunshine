# season.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from typing import List, Optional, TYPE_CHECKING
from datetime import datetime
from logging import error

from .episode import Episode
from .local import LocalStorage
from ..sunshine import SCHEMA, BASE_URL

if TYPE_CHECKING:
    from rdflib import Graph, URIRef
    from .storage import Storage


class Season:
    """
    Data model of a TV show's season.
    """

    def __init__(self, iri: URIRef, graph: Optional[Graph] = None):
        self._iri: URIRef = iri
        self._graph: Graph
        self._storage: Storage = LocalStorage.get_instance()
        self._graph: Graph

        if graph is None:
            self._graph = self._storage.retrieve_season(iri)
        else:
            self._graph = graph

    @property
    def identifier(self) -> str:
        """
        Season identifier, provided by the data source
        """
        if not hasattr(self, '_identifier'):
            self._identifier: str = self._graph.value(self._iri,
                                                      SCHEMA.identifier,
                                                      any=False).toPython()
        return self._identifier

    @property
    def title(self) -> str:
        """
        Season title
        """
        if not hasattr(self, '_title'):
            self._title: str = self._graph.value(self._iri,
                                                 SCHEMA.name,
                                                 any=False).toPython()
        return self._title

    @property
    def number(self) -> int:
        """
        Season number
        """
        if not hasattr(self, '_number'):
            self._number: int = self._graph.value(self._iri,
                                                  SCHEMA.seasonNumber,
                                                  any=False).toPython()
        return self._number

    @property
    def date(self) -> Optional[datetime]:
        """
        Season air date
        """
        if not hasattr(self, '_date'):
            self._date: Optional[datetime] = None
            try:
                self._date = self._graph.value(self._iri,
                                               SCHEMA.datePublished,
                                               any=False).toPython()
                if isinstance(self._date, datetime):
                    self._date = self._date.replace(tzinfo=None)
                else:
                    self._date = None
                    error(f'Invalid date for show ({self.iri}): '
                          f'"{self._date}"')
            except AttributeError:
                pass
        return self._date

    @property
    def description(self) -> Optional[str]:
        """
        Season description
        """
        if not hasattr(self, '_description'):
            try:
                self._description: Optional[str] = None
                self._description = self._graph.value(self._iri,
                                                      SCHEMA.description,
                                                      any=False).toPython()
            except AttributeError:
                pass
        return self._description

    @property
    def number_of_episodes(self) -> Optional[int]:
        """
        Number of episodes of the season
        """
        if not hasattr(self, '_number_of_episodes'):
            try:
                self._number_of_episodes: Optional[List[Season]] = None
                episodes_iri = self._graph.objects(self._iri,
                                                   SCHEMA.episode)
                self._number_of_episodes = sum(1 for iri in episodes_iri)
            except AttributeError:
                pass
        return self._number_of_episodes

    @property
    def episodes(self) -> Optional[Episode]:
        """
        List of episodes of the season
        """
        if not hasattr(self, '_episodes'):
            try:
                self._episodes: Optional[List[Episode]] = None
                episodes_iri = self._graph.objects(self._iri,
                                                   SCHEMA.episode)
                for iri in episodes_iri:
                    if self._episodes is None:
                        self._episodes = []
                    self._episodes.append(Episode(iri))
                self._episodes.sort(key=lambda episode: episode.number)
            except AttributeError:
                pass
        return self._episodes

    @property
    def watched(self) -> bool:
        """
        If the season was fully watched or not
        """
        if self.episodes is not None:
            today = datetime.now()
            for episode in filter(lambda e: (e.date is not None
                                             and e.date <= today),
                                             self.episodes):
                date: Optional[datetime] = episode.date

                if not episode.watched:
                    return False
            return True

        # Season has no episodes yet, cannot be fully watched
        return False

    @watched.setter
    def watched(self, state: bool) -> None:
        """
        Update the season watch state
        """
        if self.episodes is not None:
            for episode in self.episodes:
                episode.watched = state

    @property
    def iri(self) -> URIRef:
        """
        IRI of the season.
        """
        return self._iri
