# search_result.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from typing import Optional, List, TYPE_CHECKING

if TYPE_CHECKING:
    from datetime import datetime
    from .storage import Storage


class SearchResult:
    """
    Data model for a search result of TV shows.
    """

    def __init__(self, iri: URIRef, title: str, date: Optional[datetime] = None):
        self._iri: URIRef = iri
        self._title: str = title
        self._date: Optional[datetime] = date

    @property
    def iri(self) -> str:
        """
        Show IRI
        """
        return self._iri

    @property
    def title(self) -> str:
        """
        Show title
        """
        return self._title

    @property
    def date(self) -> Optional[datetime]:
        """
        Air date of the show
        """
        return self._date
