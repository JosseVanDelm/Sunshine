# kg.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from datetime import datetime
from typing import List, Optional, TYPE_CHECKING
from logging import info, debug
from pathlib import Path
from requests import Session
from requests_cache import CachedSession
from rdflib import Graph, URIRef, Literal, RDF
from rdflib.namespace import XSD
from xdg import BaseDirectory
from tempfile import NamedTemporaryFile

from .local import LocalStorage
from .datasource import Datasource
from .show import Show
from .season import Season
from .episode import Episode
from .search_result import SearchResult
from ..sunshine import SCHEMA, BASE_URL, STORAGE_DIR, RDF_FORMAT

if TYPE_CHECKING:
    from .storage import Storage


class KG(Datasource):
    """
    Datasource implementation of the Sunshine Knowledge Graph
    for retrieving TV shows information.
    """

    def __init__(self):
        super().__init__()
        self._storage: Storage = LocalStorage.get_instance()
        cache_path: Path = Path(BaseDirectory.save_cache_path(STORAGE_DIR))
        self._session: Session = CachedSession(str(cache_path))
        info(f'Cache installed at {cache_path}')

    def search(self, query) -> List[Show]:
        """
        Search for TV shows on The Movie DB.
        """
        shows: List = []

        debug('Executing search against LDES search tree')

        raise NotImplementedError('LDES search tree not implemented yet.')

    def show(self, iri: URIRef, refresh: bool = False) -> Show:
        """
        Retrieve all information about a show from the Sunshine KG.
        """
        g: Graph = Graph()
        temp_file: NamedTemporaryFile = NamedTemporaryFile()
        response: Response

        debug(f'Retrieving show data for show {iri}')

        # Download and read graph from Sunshine KG
        # RDFLib can do this behind the scenes but without support for caching
        response = self._session.get(str(iri), stream=True)

        with open(temp_file.name, 'wb') as f:
            for chunk in response:
                f.write(chunk)
        g.parse(temp_file, format=RDF_FORMAT)

        g_show = Graph()
        for t in g.triples((iri, None, None)):
            g_show.add(t)

        # Store seasons
        season_iris = g.objects(iri, SCHEMA.containsSeason)
        for season_iri in season_iris:
            g_season: Graph = Graph()

            for t in g.triples((season_iri, None, None)):
                g_season.add(t)

            self._storage.store_season(season_iri, g_season)

            # Store episodes
            episode_iris = g.objects(season_iri, SCHEMA.episode)
            for episode_iri in episode_iris:
                g_episode: Graph = Graph()
                watched_iri: URIRef

                for t in g.triples((episode_iri, None, None)):
                    g_episode.add(t)

                # Watch status, only for new shows
                watched_iri = URIRef(f'{episode_iri.toPython()}/watched')
                g_episode.add((watched_iri,
                               SCHEMA.actionStatus,
                               SCHEMA.PotentialActionStatus))
                g_episode.add((watched_iri,
                               SCHEMA.object,
                               episode_iri))
                g_episode.add((episode_iri,
                               SCHEMA.potentialAction,
                               watched_iri))

                self._storage.store_episode(episode_iri, g_episode, refresh)

        # Store locally for later re-use
        self._storage.store_show(iri, g_show)

        return Show(iri, g_show)
