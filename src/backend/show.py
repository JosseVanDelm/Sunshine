# show.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from typing import Optional, List, TYPE_CHECKING
from datetime import datetime
from logging import error

from .season import Season
from .local import LocalStorage
from ..sunshine import SCHEMA

if TYPE_CHECKING:
    from rdflib import URIRef, Graph
    from .storage import Storage


class Show:
    """
    Data model for a TV show with seasons and episodes.
    """

    def __init__(self, iri: URIRef, graph: Optional[Graph] = None):
        self._iri: URIRef = iri
        self._storage: Storage = LocalStorage.get_instance()
        self._graph: Graph

        if graph is None:
            self._graph = self._storage.retrieve_show(iri)
        else:
            self._graph = graph

    def __eq__(self, other):
        """
        Shows with the same IRI are equal.
        """
        return self.iri == other.iri

    @property
    def identifier(self) -> str:
        """
        Show identifier
        """
        if not hasattr(self, '_identifier'):
            self._identifier: str = self._graph.value(self._iri,
                                                      SCHEMA.identifier,
                                                      any=False).toPython()
        return self._identifier

    @property
    def title(self) -> str:
        """
        Show title
        """
        if not hasattr(self, '_title'):
            self._title: str = self._graph.value(self._iri,
                                                 SCHEMA.name,
                                                 any=False).toPython()
        return self._title

    @property
    def date(self) -> Optional[datetime]:
        """
        Air date of the show
        """
        if not hasattr(self, '_date'):
            self._date: Optional[datetime] = None
            try:
                self._date = self._graph.value(self._iri,
                                               SCHEMA.datePublished,
                                               any=False).toPython()
                if isinstance(self._date, datetime):
                    self._date = self._date.replace(tzinfo=None)
                else:
                    self._date = None
                    error(f'Invalid date for show ({self.iri}): '
                          f'"{self._date}"')
            except AttributeError:
                pass
        return self._date

    @property
    def description(self) -> Optional[str]:
        """
        Show description
        """
        if not hasattr(self, '_description'):
            try:
                self._description: Optional[str] = None
                self._description = self._graph.value(self._iri,
                                                      SCHEMA.description,
                                                      any=False).toPython()
            except AttributeError:
                pass
        return self._description

    @property
    def cover_url(self) -> Optional[str]:
        """
        Show cover URL
        """
        if not hasattr(self, '_cover_url'):
            try:
                self._cover_url: Optional[str] = None
                self._cover_url = self._graph.value(self._iri,
                                                    SCHEMA.image,
                                                    any=False).toPython()
            except AttributeError:
                pass
        return self._cover_url

    @property
    def cover_path(self) -> Optional[str]:
        """
        Show cover path on disk
        """
        if not hasattr(self, '_cover_path') and self.cover_url is not None:
            self._cover_path: Optional[str]
            self._cover_path = self._storage.retrieve_cover(self._iri,
                                                            self.cover_url)

        return self._cover_path

    @property
    def watched(self) -> bool:
        """
        If show was fully watched or not
        """
        if self.seasons is not None:
            for season in self.seasons:
                if not season.watched:
                    return False
            return True

        # Show cannot be fully watched if there were no seasons or episodes yet
        return False

    @watched.setter
    def watched(self, state: bool) -> None:
        """
        Update the show watch state
        """
        if self.seasons is not None:
            for season in self.seasons:
                season.watched = state

    @property
    def seasons(self) -> Optional[List[Season]]:
        """
        List of seasons of the show
        """
        if not hasattr(self, '_seasons'):
            try:
                self._seasons: Optional[List[Season]] = None
                seasons_iri = self._graph.objects(self._iri,
                                                  SCHEMA.containsSeason)
                for iri in seasons_iri:
                    if self._seasons is None:
                        self._seasons = []
                    self._seasons.append(Season(iri))
                self._seasons.sort(key=lambda season: season.number)
            except AttributeError:
                pass
        return self._seasons

    @property
    def episodes(self) -> Optional[List[Episode]]:
        """
        List of episodes of the show
        """
        episodes: Optional[List[Episode]] = None
        if self.seasons is not None:
            for season in self.seasons:
                if season.episodes is not None:
                    if episodes is None:
                        episodes = []
                    episodes += season.episodes
            return episodes

        # No seasons, so no episodes either
        return None

    @property
    def number_of_seasons(self) -> Optional[int]:
        """
        Number of seasons of the show
        """
        if not hasattr(self, '_number_of_seasons'):
            try:
                self._number_of_seasons: Optional[List[Season]] = None
                seasons_iri = self._graph.objects(self._iri,
                                                  SCHEMA.containsSeason)
                self._number_of_seasons = sum(1 for iri in seasons_iri)
            except AttributeError:
                pass
        return self._number_of_seasons

    @property
    def number_of_episodes(self) -> Optional[int]:
        """
        Number of episodes of the show
        """
        number_of_episodes: int = 0

        if self.seasons is None:
            return None

        for season in self.seasons:
            if season.number_of_episodes is not None:
                number_of_episodes += season.number_of_episodes

        return number_of_episodes

    @property
    def iri(self) -> URIRef:
        """
        IRI of the show
        """
        return self._iri

    def prefetch(self) -> None:
        self.title
        self.date
        self.description
        self.cover_url
        self.cover_path
        self.watched
        self.seasons
        self.episodes
        self.number_of_seasons
        self.number_of_episodes
