# local.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from typing import List, TYPE_CHECKING
from os import makedirs
from xdg import BaseDirectory
from pathlib import Path
from logging import debug
from rdflib import URIRef, Graph
from requests import get
from urllib.parse import urlparse

from .storage import Storage
from ..sunshine import STORAGE_DIR, STORAGE_DIR_SHOW, STORAGE_DIR_COVER, \
                       STORAGE_DIR_SEASON, STORAGE_DIR_EPISODE, BASE_URL, \
                       RDF_FORMAT, SCHEMA

if TYPE_CHECKING:
    from .datasource import Datasource

    from .show import Show
    from .season import Season
    from .episode import Episode
    from requests import Response


class LocalStorage(Storage):
    __instance = None

    @staticmethod
    def get_instance():
        """
        Singleton design pattern
        """
        if LocalStorage.__instance is None:
            LocalStorage()

        return LocalStorage.__instance

    def __init__(self):
        # Singleton design pattern
        if LocalStorage.__instance is not None:
            raise Exception('This class is a singleton!')
        else:
            LocalStorage.__instance = self

        super().__init__()
        self._dir_path: Path = Path(BaseDirectory.save_data_path(STORAGE_DIR))
        self._dir_path = self._dir_path.absolute()

    def _show_path(self, iri: URIRef) -> Path:
        path: Path
        show_id: str
        iri_path: str

        # Determine local storage path for show
        # /show/$show_id
        iri_path = urlparse(iri.toPython()).path
        show_id = iri_path.split('/')[2]
        path = self._dir_path.joinpath(f'{STORAGE_DIR_SHOW}/{show_id}') \
                             .absolute()
        return path

    def _season_path(self, iri: URIRef) -> Path:
        path: Path
        season_id: str
        iri_path: str

        # Determine local storage path for season
        # /show/$show_id/season/$season_id
        iri_path = urlparse(iri.toPython()).path
        season_id = iri_path.split('/')[4]
        path = self._dir_path.joinpath(f'{STORAGE_DIR_SEASON}/{season_id}') \
                             .absolute()
        return path

    def _episode_path(self, iri: URIRef) -> Path:
        path: Path
        episode_id: str
        iri_path: str

        # Determine local storage path for episode
        # /show/$show_id/season/$season_id/episode/$episode_id
        iri_path = urlparse(iri.toPython()).path
        episode_id = iri_path.split('/')[6]
        path = self._dir_path.joinpath(f'{STORAGE_DIR_EPISODE}/{episode_id}') \
                             .absolute()
        return path

    def store_show(self, iri: URIRef, g: Graph) -> None:
        path: Path

        debug(f'Storing show {iri}')
        path = self._show_path(iri)
        makedirs(str(path.parent), exist_ok=True)
        g.serialize(path, format=RDF_FORMAT)

    def store_season(self, iri: URIRef, g: Graph) -> None:
        path: Path

        debug(f'Storing season {iri}')
        path = self._season_path(iri)
        makedirs(str(path.parent), exist_ok=True)
        g.serialize(path, format=RDF_FORMAT)

    def store_episode(self, iri: URIRef, g: Graph, refresh: bool = False) -> None:
        path: Path

        debug(f'Storing episode {iri}, with refresh? {refresh}')
        path = self._episode_path(iri)
        makedirs(str(path.parent), exist_ok=True)

        # Save watch status before refreshing episode
        if refresh:
            print('REFRESHING... Retrieving current watch status and copy it')
            g_show: Graph = Graph()
            watched_iri: URIRef = URIRef(f'{iri.toPython()}/watched')
            g_show.parse(path, format=RDF_FORMAT)
            watch_action_iri: URIRef = g_show.value(iri,
                                                    SCHEMA.potentialAction,
                                                    any=False)
            watch_status: URIRef = g_show.value(watch_action_iri,
                                                SCHEMA.actionStatus,
                                                any=False)
            g.set((watch_action_iri, SCHEMA.actionStatus, watch_status))

        g.serialize(path, format=RDF_FORMAT)

    def retrieve_shows(self) -> List[URIRef]:
        shows: List[URIRef] = []

        for path in self._dir_path.glob(f'{STORAGE_DIR_SHOW}/*'):
            g: Graph = Graph()
            iri: URIRef = URIRef(str(path.absolute()) \
                                     .replace(str(self._dir_path), BASE_URL))

            debug(f'Loading show {iri} from file: {str(path)}')
            shows.append(iri)

        return shows

    def retrieve_show(self, iri: URIRef) -> Graph:
        """
        Retrieve a show as RDF from the storage.
        """
        path: Path
        g: Graph = Graph()

        path = self._show_path(iri)
        debug(f'Loading show from file: {str(path)}')

        return g.parse(str(path), format=RDF_FORMAT)

    def retrieve_season(self, iri: URIRef) -> Graph:
        """
        Retrieve a season as RDF from the storage.
        """
        path: Path
        g: Graph = Graph()

        path = self._season_path(iri)
        debug(f'Loading season from file: {str(path)}')

        return g.parse(str(path), format=RDF_FORMAT)

    def retrieve_episode(self, iri: URIRef) -> Graph:
        """
        Retrieve a episode as RDF from the storage.
        """
        path: Path
        g: Graph = Graph()

        path = self._episode_path(iri)
        debug(f'Loading episode from file: {str(path)}')

        return g.parse(str(path), format=RDF_FORMAT)

    def retrieve_cover(self, iri: URIRef, url: str) -> Path:
        """
        Retrieve a cover image by URL and save it into storage.
        """
        path: Path = self._dir_path.joinpath(str(iri) \
                                             .replace(f'{BASE_URL}/show',
                                                      f'{STORAGE_DIR_COVER}')) \
                                             .absolute().resolve()
        makedirs(str(path.parent), exist_ok=True)

        # Cover may exist already in storage, return path in that case
        if path.exists():
            return path

        # Not downloaded yet, get cover and store it locally
        response: Response = get(url, stream=True)
        with open(path, 'wb') as f:
            for chunk in response:
                f.write(chunk)

        return path

    @property
    def has_shows(self) -> bool:
        # If we have at least one show, return True
        try:
            next(self._dir_path.glob(f'{STORAGE_DIR_SHOW}/*'))
            return True
        except StopIteration:
            return False
