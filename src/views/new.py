# new.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk
from typing import List, Tuple, TYPE_CHECKING
from datetime import datetime, timedelta

from ..widgets.new_episode import NewEpisodeWidget

if TYPE_CHECKING:
    from ..backend.show import Show
    from ..backend.episode import Episode
    from ..backend.storage import Storage


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/new.ui')
class NewView(Gtk.ScrolledWindow):
    __gtype_name__ = 'NewView'

    today_list: Gtk.Widget = Gtk.Template.Child()
    today_box: Gtk.Widget = Gtk.Template.Child()
    yday_box: Gtk.Widget = Gtk.Template.Child()
    week_box: Gtk.Widget = Gtk.Template.Child()
    month_box: Gtk.Widget = Gtk.Template.Child()
    yday_list: Gtk.Widget = Gtk.Template.Child()
    week_list: Gtk.Widget = Gtk.Template.Child()
    month_list: Gtk.Widget = Gtk.Template.Child()
    placeholder: Gtk.Widget = Gtk.Template.Child()
    placeholder_title_label: Gtk.Widget = Gtk.Template.Child()
    placeholder_description_label: Gtk.Widget = Gtk.Template.Child()
    placeholder_image: Gtk.Widget = Gtk.Template.Child()

    def __init__(self, storage: Storage, **kwargs):
        super().__init__(**kwargs)
        self._shows: List[Show] = []
        self._storage: Storage = storage
        self.set_name('new')
        self.show_all()

    def sort_list(self):
        today: datetime = datetime.now()
        today_list: List[Tuple[Show, Episode]] = []
        yday_list: List[Tuple[Show, Episode]] = []
        week_list: List[Tuple[Show, Episode]] = []
        month_list: List[Tuple[Show, Episode]] = []
        has_children: bool = False

        # Place episodes in the right today/yesterday/week/month list
        for show in filter(lambda s: s.watched == False, self._shows):
            # Filter episodes by date, unwatched and not in the future
            for episode in filter(lambda e: (e.date is not None
                                             and e.date <= today
                                             and e.watched == False),
                                             show.episodes):
                if episode.date >= (today - timedelta(days=1)):
                    today_list.append((show, episode))
                elif episode.date >= (today - timedelta(days=2)):
                    yday_list.append((show, episode))
                elif episode.date >= (today - timedelta(days=7)):
                    week_list.append((show, episode))
                elif episode.date >= (today - timedelta(days=31)):
                    month_list.append((show, episode))

        # Update list views
        lists = [(today_list, self.today_list, self.today_box),
                 (yday_list, self.yday_list, self.yday_box),
                 (week_list, self.week_list, self.week_box),
                 (month_list, self.month_list, self.month_box)]

        for data, widget, box in lists:
            # If there's no data, skip
            if not data:
                box.hide()
                continue
            else:
                has_children = True

            # Sort new episodes by date
            data.sort(key=lambda episode: episode[1].date)

            for show, episode in data:
                added: bool = False
                children: List[Gtk.Widget] = widget.get_children()
                w: NewEpisodeWidget = NewEpisodeWidget(show, episode,
                                                       self._storage, self)

                # No children yet, directly add widget
                if not children:
                    widget.add(w)
                    continue

                # Skip if already in list
                try:
                    next(filter(lambda c: c.iri == episode.iri, children))
                    continue
                except StopIteration:
                    pass

                # Try to insert widget in list
                for index, child in enumerate(children):
                    if episode.date < child.date:
                        widget.insert(w, index)
                        added = True
                        break

                if added:
                    continue

                # Widget not inserted, append at the end of
                widget.add(w)

            # We have children, show box
            box.show()

        # Show placeholder when we have no new episodes anymore
        self.placeholder.set_visible(not has_children)


    def add_show(self, show: Show):
        self._shows.append(show)
        self.sort_list()

    def replace_show(self, show: Show):
        self._shows.remove(show)
        self.add_show(show)

    def loading(self, state: bool):
        if state:
            self.placeholder_title_label.set_visible(False)
            self.placeholder_description_label.set_visible(False)
            self.placeholder_image.get_style_context().remove_class('dim-label')
        else:
            self.placeholder_title_label.set_visible(True)
            self.placeholder_description_label.set_visible(True)
            self.placeholder_image.get_style_context().add_class('dim-label')

    def refresh(self):
        lists = [(self.today_list, self.today_box),
                 (self.yday_list, self.yday_box),
                 (self.week_list, self.week_box),
                 (self.month_list, self.month_box)]

        has_children: bool = False
        for widget, box in lists:
            for episode in widget.get_children():
                if episode.watched:
                    widget.remove(episode)
                else:
                    episode.refresh()
            # All episodes watched for list, hide it again
            if widget.get_children():
                has_children = True
            else:
                box.hide()

        self.placeholder.set_visible(not has_children)
