# library.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk
from typing import TYPE_CHECKING

from ..widgets.show import ShowWidget

if TYPE_CHECKING:
    from ..backend.show import Show
    from ..backend.storage import Storage
    from ..page_stack import PageStack


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/library.ui')
class LibraryView(Gtk.ScrolledWindow):
    __gtype_name__ = 'LibraryView'

    library_list: Gtk.Widget = Gtk.Template.Child()
    placeholder: Gtk.Widget = Gtk.Template.Child()
    placeholder_title_label: Gtk.Widget = Gtk.Template.Child()
    placeholder_description_label: Gtk.Widget = Gtk.Template.Child()
    placeholder_image: Gtk.Widget = Gtk.Template.Child()

    def __init__(self, page_stack: PageStack, storage: Storage, **kwargs):
        super().__init__(**kwargs)
        self._page_stack: PageStack = page_stack
        self._storage: Storage = storage
        self.set_name('library')
        self.show_all()

    def add_show(self, show: Show):
        self.library_list.add(ShowWidget(show, self._page_stack, self._storage))
        self.library_list.set_visible(True)
        self.placeholder.set_visible(False)

    def replace_show(self, show: Show):
        for s in self.library_list.get_children():
            if s.iri == show.iri:
                self.library_list.remove(s)
                break
        self.add_show(show)

    def loading(self, state: bool):
        if state:
            self.placeholder_title_label.set_visible(False)
            self.placeholder_description_label.set_visible(False)
            self.placeholder_image.get_style_context().remove_class('dim-label')
        else:
            self.placeholder_title_label.set_visible(True)
            self.placeholder_description_label.set_visible(True)
            self.placeholder_image.get_style_context().add_class('dim-label')

    def refresh(self):
        for show in self.library_list.get_children():
            show.refresh()
