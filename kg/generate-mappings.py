#!/usr/bin/env python3

# generate-mappings.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gzip
import json
import requests_cache
from typing import List, Optional
from pathlib import Path
from requests import get, Response
from time import time
from os import makedirs, getenv
from multiprocessing import Pool
from tqdm import tqdm

DAILY_EXPORT_URL = 'http://files.tmdb.org/p/exports/tv_series_ids_11_24_2021.json.gz'
EXPORT_ARCHIVE = 'tv_series_ids_11_24_2021.json.gz'
SHOW_DATA_DIR = 'data/show'
SEASON_DATA_DIR = 'data/season'
EPISODE_DATA_DIR = 'data/episode'
SHOW_KG_DIR = 'kg/show'
MAPPINGS_DIR = 'mappings/generated'
API_BASE_URL = 'https://api.tmdb.org/3'
API_KEY = getenv('TMDB_API_KEY')
LANGUAGE = 'en-US'
SHOW_MAPPING_TEMPLATE = 'mappings/show_template.rml.ttl'
SEASON_MAPPING_TEMPLATE = 'mappings/season_template.rml.ttl'
EPISODE_MAPPING_TEMPLATE = 'mappings/episode_template.rml.ttl'


def generate_mapping(show_id: int) -> Optional[Path]:
    """
    Generate RML mapping rules by filling in the mapping templates
    with data from the TMDB API.
    """
    show_title: str
    number_of_seasons: int
    number_of_episodes: int
    data: dict
    r: Response
    path: Path = Path(MAPPINGS_DIR).joinpath(f'{show_id}.rml.ttl')

    if path.exists():
        return path

    # Get data from API
    for i in range(0, 5):
        try:
            r = get(f'{API_BASE_URL}/tv/{show_id}',
                    params = {'api_key': API_KEY, 'language': LANGUAGE})
            if r.status_code != 200:
                print(f'Show request failed: HTTP {r.status_code}')
                return None
            break
        except ConnectionResetError:
            print('Connection Reset received, sleeping for 5s')
            time.sleep(5)
    data = r.json()
    show_title = data['name']
    with open(Path(SHOW_DATA_DIR).joinpath(f'{data["id"]}.json'), 'w') as f:
        json.dump(data, f)

    # Show data
    with open(SHOW_MAPPING_TEMPLATE, 'r') as f1:
        with open(str(path), 'w') as f2:
            for l in f1.readlines():
                if '$SHOW_ID' in l:
                    l = l.replace('$SHOW_ID', str(show_id))
                f2.write(l)

    if not 'seasons' in data:
        print('No seasons, skipped')

    # Seasons data
    for season in data['seasons']:
        season_id = season['id']
        season_number = season['season_number']

        # Get data from API
        for i in range(0, 5):
            try:
                r = get(f'{API_BASE_URL}/tv/{show_id}/season/{season_number}',
                        params = {'api_key': API_KEY, 'language': LANGUAGE})
                if r.status_code != 200:
                    print('Season request failed')
                    continue
                break
            except ConnectionResetError:
                print('Connection Reset received, sleeping for 5s')
                time.sleep(5)

        data = r.json()
        with open(Path(SEASON_DATA_DIR).joinpath(f'{data["id"]}.json'), 'w') as f:
            json.dump(data, f)

        # Fill in template
        with open(SEASON_MAPPING_TEMPLATE, 'r') as f1:
            with open(str(path), 'a+') as f2:
                for l in f1.readlines():
                    if '$SHOW_ID' in l:
                        l = l.replace('$SHOW_ID', str(show_id))
                    if '$SEASON_NUMBER' in l:
                        l = l.replace('$SEASON_NUMBER', str(season_number))
                    if '$SEASON_ID' in l:
                        l = l.replace('$SEASON_ID', str(data['id']))
                    f2.write(l)

        if not 'episodes' in data:
            print('No episodes, skipped')

        # Episode data
        for episode in data['episodes']:
            episode_number = episode['episode_number']

            if r.status_code != 200:
                print('Episode request failed')
                continue
            with open(Path(EPISODE_DATA_DIR).joinpath(f'{episode["id"]}.json'), 'w') as f:
                json.dump(episode, f)

            with open(EPISODE_MAPPING_TEMPLATE, 'r') as f1:
                with open(str(path), 'a+') as f2:
                    for l in f1.readlines():
                        if '$SHOW_ID' in l:
                            l = l.replace('$SHOW_ID', str(show_id))
                        if '$SEASON_ID' in l:
                            l = l.replace('$SEASON_ID', str(season_id))
                        if '$SEASON_NUMBER' in l:
                            l = l.replace('$SEASON_NUMBER', str(season_number))
                        if '$EPISODE_NUMBER' in l:
                            l = l.replace('$EPISODE_NUMBER', str(episode_number))
                        if '$EPISODE_ID' in l:
                            l = l.replace('$EPISODE_ID', str(episode['id']))
                        f2.write(l)
    return path

if __name__ == '__main__':
    print('*' * 80)
    print('Generating RDF knowledge graph from The Movie DB')

    # Cache
    requests_cache.install_cache('tmdb_api_http_cache')

    # Retrieve list of show ids
    response: Response = get(DAILY_EXPORT_URL, stream=True)
    show_ids: List = []
    with open(EXPORT_ARCHIVE, 'wb') as f:
        for chunk in response:
            f.write(chunk)

    with gzip.open(EXPORT_ARCHIVE, 'r') as f:
        for line in f:
            s = json.loads(line)
            show_ids.append(s['id'])

    # Create data dirs
    makedirs(SHOW_DATA_DIR, exist_ok=True)
    makedirs(SEASON_DATA_DIR, exist_ok=True)
    makedirs(EPISODE_DATA_DIR, exist_ok=True)
    makedirs(SHOW_KG_DIR, exist_ok=True)
    makedirs(MAPPINGS_DIR, exist_ok=True)
    
    print(f'Starting to map {len(show_ids)} shows')
    print('*' * 80)

    # Generate mappings
    for show_id in tqdm(show_ids, total=len(show_ids)):
        generate_mapping(show_id)
