# Generating the Sunshine Knowledge Graph

Sunshine uses a Knowledge Graph in RDF (N-Triples) to store, 
retrieve and understand data about TV shows, seasons and episodes.
This Knowledge Graph is generated using [RML mapping rules](https://rml.io)
from data of [The Movie DB](https://themoviedb.org)
and available at [https://sunshine.dylanvanassche.be](https://sunshine.dylanvanassche.be).
Each IRI of Sunshine actually exist, following the Semantic Web's 
[Linked Data principles](https://www.w3.org/DesignIssues/LinkedData.html).

## Generate RML mapping rules

To generate the RML mapping rules, you need to get an API key from The Movie DB (TMDB).
For more information, head over to the [TMDB API documentation](https://www.themoviedb.org/documentation/api).

```
pipenv install
pipenv shell
export TMDB_API_KEY='<TMDB API KEY>'
./generate-mappings.py
```

## Execute RML mapping rules

To execute the RML mapping rules, the [RMLMapper](https://github.com/rmlio/rmlmapper-java) is used.
The RMLMapper Jar is automatically downloaded when the script is executed.
The script iterates over all the generated mappings and executes the RMLMapper on multiple CPU cores.

```
pipenv install
pipenv shell
./execute-mappings.py
```

## TMDB attribution

> This product uses the TMDB API but is not endorsed or certified by TMDB.

![New view](tmdb.png "TMDB attribution logo.")
